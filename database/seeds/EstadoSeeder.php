<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('estados')->insert([
            'descricao' => 'Paraná',
            'uf' => 'PR'
        ]);

        DB::table('estados')->insert([
            'descricao' => 'São Paulo',
            'uf' => 'SP'
        ]);

        DB::table('estados')->insert([
            'descricao' => 'Rio de Janeiro',
            'uf' => 'RJ'
        ]);

        DB::table('estados')->insert([
            'descricao' => 'Santa Catarina',
            'uf' => 'SC'
        ]);
    }
}
