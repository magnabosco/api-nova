<?php

use Illuminate\Database\Seeder;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('produtos')->insert([
            'descricao' => 'Camiseta Exeplo Primeiro Produto',
            'codcategoria' => 1,
            'valor' => 69.99
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Camiseta Exeplo Primeiro Produto',
            'codcategoria' => 4,
            'valor' => 49.99
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Camiseta Exeplo Segundo Produto',
            'codcategoria' => 1,
            'valor' => 80.11
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Camiseta Exeplo Segundo Produto',
            'codcategoria' => 4,
            'valor' => 77.42
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calçado Exeplo Primeiro Produto',
            'codcategoria' => 2,
            'valor' => 99.99
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calçado Exeplo Primeiro Produto',
            'codcategoria' => 5,
            'valor' => 100.00
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calçado Exeplo Segundo Produto',
            'codcategoria' => 2,
            'valor' => 50.00
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calçado Exeplo Segundo Produto',
            'codcategoria' => 5,
            'valor' => 2.99
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calça Exeplo Primeiro Produto',
            'codcategoria' => 3,
            'valor' => 99.32
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calça Exeplo Primeiro Produto',
            'codcategoria' => 6,
            'valor' => 11.32
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calça Exeplo Segundo Produto',
            'codcategoria' => 3,
            'valor' => 33.32
        ]);

        DB::table('produtos')->insert([
            'descricao' => 'Calça Exeplo Segundo Produto',
            'codcategoria' => 6,
            'valor' => 55.32
        ]);
    }
}
