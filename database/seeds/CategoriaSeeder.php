<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categorias')->insert([
            'nomecategoria' => 'Camisetas Femininas',
        ]);

        DB::table('categorias')->insert([
            'nomecategoria' => 'Calçados Femininos',
        ]);

        DB::table('categorias')->insert([
            'nomecategoria' => 'Calças Femininas',
        ]);

        DB::table('categorias')->insert([
            'nomecategoria' => 'Camisetas Masculinas',
        ]);

        DB::table('categorias')->insert([
            'nomecategoria' => 'Calçados Masculinos',
        ]);

        DB::table('categorias')->insert([
            'nomecategoria' => 'Calças Masculinas',
        ]);
    }
}
