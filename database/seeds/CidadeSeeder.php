<?php

use Illuminate\Database\Seeder;

class CidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cidades')->insert([
            'cidade' => 'São João',
            'id_estado' => 1
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'Itapejara',
            'id_estado' => 1
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'São Jorge',
            'id_estado' => 1
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'Vere',
            'id_estado' => 1
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'São Paulo',
            'id_estado' => 2
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'Rio de Janeiro',
            'id_estado' => 3
        ]);

        DB::table('cidades')->insert([
            'cidade' => 'São Lorenço',
            'id_estado' => 4
        ]);
    }
}
