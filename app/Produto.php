<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //
    protected $fillable = ['descricao','codcategoria','valor'];

    public function categoriaFuncao(){
        return $this->belongsTo('App\Categoria', 'codcategoria', 'id');
    }
}
