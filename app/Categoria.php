<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $fillable = ['nomecategoria'];

    public function produtoFuncao(){
        return $this->hasMany('App\Produto', 'codcategoria', 'id');
    }
}
