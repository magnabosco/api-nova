<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;


class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qtd = $request->input('qtd');
        return response()->json( Estado::with('cidadeFuncao')->paginate($qtd) );
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $estado = new Estado();
        $estado->fill($request->all());
        $estado->save();
        return response()->json($estado);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estado = Estado::with('cidadeFuncao')->find($id);
        return response()->json($estado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = Estado::find($id);
        $estado->fill( $request->all() );
        $estado->save();

        return $estado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = Estado::find($id);
        $estado->delete();
        return $estado;
    }

    public function teste()
    {
        return "Metodo teste funcionou";
    }

}
