<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use Validator;

class CategoriaController extends Controller
{
    private $atributos = ['nomecategoria'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qtd = $request->input('qtd');
        return Categoria::with('produtoFuncao')->paginate($qtd);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $categoria = new Categoria();
//        $categoria->fill($request->all());
//        $categoria->save();
//        return $categoria;

        try{
            //Chamar validacao
            $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $categoria = new Categoria();
            $categoria->fill( $request->all() );
            $categoria->save();

            //Verifica se cadastrou a cidade no banco
            if( $categoria ){
                return response()->json( [$categoria], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar Categoria"], 400 );
            }

            return $categoria;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Categoria::with('produtoFuncao')->find($id);
        return $categoria;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //Chamar validacao
            $validacao = $this->validar($request);

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $categoria = Categoria::find($id);
            $categoria->fill( $request->all() );
            $categoria->save();

            //Verifica se cadastrou a cidade no banco
            if( $categoria ){
                return response()->json( [$categoria], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar Categoria"], 400 );
            }

            return $categoria;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        $categoria->delete();
        return $categoria;
    }

    public function validar( $request ){
        $validator = Validator::make($request->only( $this->atributos ),[
            'nomecategoria' => 'required|min:3|max:80',
        ]);
        return $validator;
    }
}
