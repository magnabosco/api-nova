<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstadoDBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // SELECT * FROM estado.estados;
        $estados = DB::SELECT("SELECT * FROM estados");

        // return ('TESTE SEM MODEL');
        // return json_encode($estados);

        return $estados;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // DB::insert("insert into estados (descricao, uf) values (?,?)", ["Santa Catarina", "SC"]);

        DB::insert("insert into estados (descricao, uf) values (?,?)", 
            [$request->input('descricao'), $request->input('uf')]);

        return "Inserido com sucesso";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estados = DB::select("SELECT * FROM estado.estados where id = ?", [$id]);

        return $estados;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estados = DB::update("update estados set descricao = ?, uf = ? where estados.id", 
             [$request->input('descricao'), $request->input('uf'), $id]);

        return "Alterou com sucesso";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete("delete from estados where id = ?", [$id]);

        return "Deletou o registro $id";
    }
}
