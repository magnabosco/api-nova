<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;

class ProdutoController extends Controller
{
    //PEGUEI NO REPOSITORIO NÃO SEI PRA QUE SERVE
    private $atributos = ['descricao','codcategoria','valor'];

    public function index(Request $request)
    {
        try{
            $qtd = $request->input('qtd');
            return response()->json( Produto::with('categoriaFuncao')->paginate($qtd) , 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $produto = new Produto();
//        $produto->fill($request->all());
//        $produto->save();
//        return $produto;

        try{
            //Chamar validacao
            $validacao = $this->validar($request);
//            $validacao->after(function ($validacao) {
//                $validacao->errors()->add('descricao', 'Deve inserir uma descrição válida!');
//            });

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $produto = new Produto();
            $produto->fill( $request->all() );
            $produto->save();

            //Verifica se cadastrou a cidade no banco
            if( $produto ){
                return response()->json( [$produto], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar produto"], 400 );
            }

            return $produto;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::with('categoriaFuncao')->find($id);
        return $produto;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::find($id);
        $produto->fill( $request->all() );
        $produto->save();
        return $produto;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        $produto->delete();
        return $produto;
    }

    public function validar( $request ){
        $validator = Validator::make($request->only( $this->atributos ),[
            'descricao' => 'required|min:3|max:80',
            'codcategoria' => 'required|numeric',
            'valor' => 'required|numeric'
        ]);
        return $validator;
    }
}
