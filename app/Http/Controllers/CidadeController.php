<?php

namespace App\Http\Controllers;

use App\Cidade;
use Illuminate\Http\Request;

class CidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Cidade::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //valida se os campos foram preenchidas
        //min:3 - minimo 3 caracteres
        $this->validate(
            $request,
            [
                'cidade' => 'required|min:3',
                'id_estado' => 'required',
            ]
        );

        $cidade = new Cidade();

        $cidade->fill($request->all());

        $cidade->save();

        return $cidade;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // $cidade = Cidade::find($id);
        
        $cidade = Cidade::with('estadoFuncao')->find($id);


        return $cidade;

        //aqui estápegando o atributo do 'estado' que foi definido na model
        // return $cidade->estado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cidade = Cidade::find($id);

        $cidade->fill($request->all());

        $cidade->save();

        return $cidade;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cidade = Cidade::find($id);

        $cidade->delete();

        return $cidade;
    }
}





